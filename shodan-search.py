#!/usr/bin/env python3
# vim: ts=4 sw=4 sts=4 noet :

import shodan
import os
import time
import sys
from xdg import *

def usage():
    print("usage: {} '<shodan search string>'".format(sys.argv[0]))
    print("Searches shodan and prints information about each host found.")
    sys.exit(0)


def read_config():
    shodan_api_key = None
    config = str(xdg_config_home()) + "/shodan/api_key"
    if os.path.isfile(config):
        handle = open(config)
        fil = handle.readlines()
        handle.close()
        shodan_api_key = fil[0].strip()
    return shodan_api_key


def main(args):
    SHODAN_API_KEY = read_config()
    api = shodan.Shodan(SHODAN_API_KEY)

    try:
        # search Shodan
        results = api.search(args)
        print('Info: results found {}'.format(results['total']), file=sys.stderr)
        
        print('ip|hostnames|ports|os|vulnerabilities')
        for result in results['matches']:
            # lookup host
            host = api.host(result['ip_str'])
            
            print("{}|{}|{}|{}|{}".format(\
                    host['ip_str'], \
                    host['hostnames'], \
                    host.get('ports', 'n/a'), \
                    host.get('os', 'n/a'), \
                    host.get('vulns', 'n/a')))
            # rate limit
            time.sleep(1)

    except shodan.APIError as e:
        print('Error: {}'.format(e))

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage()

    main(sys.argv[1:])

