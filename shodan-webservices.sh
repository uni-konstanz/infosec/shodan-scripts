#!/bin/bash

set -e -o pipefail
shopt -s failglob inherit_errexit

usage() {
    echo "usage: $0 <network CIDR>"
}

fname="webservices.psv"

# error handling
command -v shodan >/dev/null 2>&1 || { echo >&2 "ERROR: shodan required. Run 'pip install shodan'"; usage; exit 1; }
if [ -z "$1" ]; then usage; exit 1; fi

# search webservices
echo "Retrieving webservices ..."
shodan search --color --fields "ip_str,hostnames,port,org,data" --separator "|" "net:${1} port:443,80" > "${fname}";

# query hosts
for i in $(cut -d'|' -f1 "${fname}" | sort -u); do 
    echo "Query host $i";
    shodan host "$i" > "$i.txt"; 
    sleep 1; 
done

# sort vulnerability
mkdir -p vulnerable
echo "Moving vulnerable hosts"
grep -lZi cve ./*.txt | xargs -0 -I{} mv -v {} vulnerable

