# shodan-scripts

Scripts to automatically query shodan for several services / devices

## Scripts  

### `webservices.sh`

Searches for services on ports 443,80, queries each hosts vulnerabilities.

    usage: ./webservices.sh <network CIDR>
    
### `printers.sh`

Searches for printers and queries each hosts vulnerabilities.

    usage: ./printers.sh <network CIDR>

